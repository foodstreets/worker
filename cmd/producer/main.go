package main

import (
	"log"

	"gitlab.com/foodstreets/worker/config"

	"gitlab.com/foodstreets/master/cmd"
	"gitlab.com/foodstreets/master/infra"

	user "gitlab.com/foodstreets/worker/sources/worker"
)

func main() {
	cmd.Execute()
	conf := config.Get()

	//Load Config
	//conf := config.Init()

	// Load RabbitMQ
	rbt := infra.InitRabbitmq(conf.RabbitMQ)
	if err := rbt.Connect(); err != nil {
		log.Fatalln(err)
	}
	defer rbt.Shutdown()

	// AMQP Setup
	userAMQP := user.NewAMQP(conf.UserAMQP, rbt)
	if err := userAMQP.Setup(); err != nil {
		log.Fatalln(err)
	}

	if err := userAMQP.Delivery(); err != nil {
		log.Fatalln(err)
	}

	forever := make(chan bool)
	<-forever
}
