package main

import (
	"log"

	user "gitlab.com/foodstreets/worker/sources/worker"

	"gitlab.com/foodstreets/worker/config"

	"gitlab.com/foodstreets/master/cmd"
	"gitlab.com/foodstreets/master/infra"
)

func main() {
	cmd.Execute()
	conf := config.Get()

	//Load Config
	//conf := config.New()

	// Load RabbitMQ
	rbt := infra.InitRabbitmq(conf.RabbitMQ)
	if err := rbt.Connect(); err != nil {
		log.Fatalln(err)
	}
	defer rbt.Shutdown()

	consumer := user.NewConsumer(conf.Consumer, rbt)
	if err := consumer.Start(); err != nil {
		log.Fatalln(err)
	}
}
