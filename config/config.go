package config

import (
	"fmt"
	"time"

	user "gitlab.com/foodstreets/worker/sources/worker"

	"sync"

	"gitlab.com/foodstreets/master/cmd"
	"gitlab.com/foodstreets/master/infra"

	"gitlab.com/foodstreets/master/config"
)

var (
	once sync.Once
	conf Config
)

type Config struct {
	config.Config
	Address     string
	HttpAddress HttpAddress
	RabbitMQ    infra.RabbitMQConfig
	UserAMQP    user.AMQPConfig
	Consumer    infra.ConsumerConfig
}

type HttpAddress struct {
	Host string
	Port int
}

func load() {
	once.Do(func() {
		gConfig := config.Load()

		if err := cmd.GetViper().Unmarshal(&conf); err != nil {
			panic(err)
		}

		conf.Config = gConfig
	})
}

func Get() Config {
	load()
	return conf
}

func New() Config {
	var conf Config

	httpAdress := fmt.Sprintf("%s:%d", conf.HttpAddress.Host, conf.HttpAddress.Port)
	conf.Address = httpAdress

	conf.RabbitMQ.Schema = "amqp"
	conf.RabbitMQ.Username = "admin"
	conf.RabbitMQ.Password = "admin"
	conf.RabbitMQ.Host = "192.168.20.34"
	conf.RabbitMQ.Port = "5672"
	conf.RabbitMQ.VHost = "my_app"
	conf.RabbitMQ.ConnectingName = "MY_APP"
	conf.RabbitMQ.ChannelNotifyTimeout = 100 * time.Millisecond
	conf.RabbitMQ.Reconnect.Interval = 500 * time.Millisecond
	conf.RabbitMQ.Reconnect.MaxAttempt = 7200

	// Config user amqp
	conf.UserAMQP.Create.ExchangeName = "worker"
	conf.UserAMQP.Create.ExchangeType = "direct"
	conf.UserAMQP.Create.RoutingKey = "create"
	conf.UserAMQP.Create.QueueName = "worker_create"

	conf.Consumer.ExchangeName = "worker"
	conf.Consumer.ExchangeType = "direct"
	conf.Consumer.RoutingKey = "create"
	conf.Consumer.QueueName = "worker_create"
	conf.Consumer.ConsumerCount = 5
	conf.Consumer.PrefetchCount = 1
	conf.Consumer.Reconnect.Interval = 1 * time.Second
	conf.Consumer.Reconnect.MaxAttempt = 60

	return conf
}
