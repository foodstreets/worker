package client

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/foodstreets/worker/config"

	"gitlab.com/foodstreets/master/model"

	"gitlab.com/foodstreets/master/infra"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type client struct {
}

var Client IClient

func init() {
	Client = &client{}
}

type IClient interface {
	CheckDiscovery(id int)
}

func (client) CheckDiscovery(id int) {
	model := model.RabbitMQ{
		Action:     model.ActionCheckDiscovery,
		Id:         id,
		ExecutedAt: time.Now(),
	}
	go func() {
		err := Send(&model)
		if err != nil {
			fmt.Println("ERR", err)
		}
	}()

}

func Send(model *model.RabbitMQ) error {
	conf := config.New()

	// Load RabbitMQ
	rbt := infra.InitRabbitmq(conf.RabbitMQ)
	if err := rbt.Connect(); err != nil {
		log.Fatalln(err)
	}
	defer rbt.Shutdown()
	channel, err := rbt.Channel()

	if err != nil {
		return errors.Wrap(err, "Failed to open channel")
	}
	defer channel.Close()

	if err := channel.Confirm(false); err != nil {
		return errors.Wrap(err, "Failed to put channel is confirm mode")
	}

	// convert struct to []bytes
	bytes, _ := json.Marshal(model)
	err = channel.Publish(
		"worker",
		"create",
		true,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			MessageId:    "A-UNIQUE-ID",
			ContentType:  "text/plain",
			Body:         bytes,
		},
	)
	if err != nil {
		return errors.Wrap(err, "Failed to publish message")
	}

	select {
	case ntf := <-channel.NotifyPublish(make(chan amqp.Confirmation, 1)):
		if !ntf.Ack {
			return errors.New("Failed to deliver msg to exchange/queue")
		}
	case <-channel.NotifyReturn(make(chan amqp.Return)):
		return errors.New("Failed to delivery msg to exchange/queue")
	case <-time.After(rbt.ChannelNotifyTimeout):
		return errors.New("Failed msg delivery confirm to exchange/queue timed out")
	}

	return nil
}
