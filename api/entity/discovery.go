package entity

import (
	"fmt"

	"gitlab.com/foodstreets/master/model"
)

type discovery struct{}

var Discovery IDiscovery

type IDiscovery interface {
	CheckDiscovery(task model.RabbitMQ) error
}

func init() {
	Discovery = &discovery{}
}

func (discovery) CheckDiscovery(task model.RabbitMQ) (err error) {
	fmt.Println("Entity-task", task)
	return
}
