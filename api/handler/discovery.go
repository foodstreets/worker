package handler

import (
	"gitlab.com/foodstreets/master/model"
)

type discovery struct{}

var Discovery IDiscovery

type IDiscovery interface {
	CheckDiscovery(task model.RabbitMQ) error
}

func init() {
	Discovery = &discovery{}
}

func (discovery) CheckDiscovery(task model.RabbitMQ) (err error) {
	return
}
