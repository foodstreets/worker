package handler

import (
	"encoding/json"

	"gitlab.com/foodstreets/master/model"

	"github.com/streadway/amqp"
)

func Init(msg *amqp.Delivery) {
	var (
		task model.RabbitMQ
	)
	err := json.Unmarshal(msg.Body, &task)
	if err != nil {
		panic(err)
		return
	}

	action := task.Action
	switch action {
	case model.ActionCheckDiscovery:
		Discovery.CheckDiscovery(task)
	}
}
